var http = require('http');
var fs = require('fs');
const GridClass = require('./grid.js');
const grid = require('./grid.js');

var tiles = 
{
    deadplayer: "☠️",
    players:
    [
        "🙂",
        "😠",
        "😭",
        "😲",
        "😍",
        "😘",
        "🤑",
        "🤔",
        "😈",
        "👼",
        "😎",
        "😬",
        "😕",
        "😵",
        "😷",
        "😜",
        "😫",
        "😝",
        "😂",
        "😞",
        "😁",
        "😄",
        "😗",
        "🤕",
        "🤨",
        "😑",
        "😧",
        "🎅",
        "🤓",
        "😶",
        "🙃",
        "🤡",
        "🤠",
        "🤩",
        "🤯",
        "🤴",
        "👽",
        "🤪",
        "🤐"
    ],
    walls:
    {
        vertical: "│",
        horizontal: "─",
        cross: "┼",
        
        horizontalLineDown: "┬",
        horizontalLineUp: "┴",
        verticalLineLeft: "┤",
        verticalLineRight: "├",

        topLeft: "┌",
        topRight: "┐",
        bottomLeft: "└",
        bottomRight: "┘"
    },
    rangeCheck:
    {
        vertical: "║",
        horizontal: "═",
        cross: "╬",

        horizontalLineDown: "╤",
        horizontalLineUp: "╧",
        verticalLineLeft: "╢",
        verticalLineRight: "╟",

        dottedVertical: "┆",
        dottedHorizontal: "┄",

        topLeft: "╔",
        topRight: "╗",
        bottomLeft: "╚",
        bottomRight: "╝"
    }
}

if (!Object.entries) {
    Object.entries = function( obj ){
      var ownProps = Object.keys( obj ),
          i = ownProps.length,
          resArray = new Array(i); // preallocate the Array!
      while (i--)
      {
        resArray[i] = [ownProps[i], obj[ownProps[i]]];
      }
      
      return resArray;
    };
  }

exports.admins = 
[
    "124136556578603009" //Timfa
]

exports.cmds = 
[
    {
        cmd: "join",
        params: "none",
        category: "main",
        execute: function(bot, info, args)
        {
            VerifySave(bot, info);
            
            bot.data[info.serverId][info.channelID].players[info.userID] = bot.data[info.serverId][info.channelID].players[info.userID] || {}

            if(!bot.data[info.serverId][info.channelID].players[info.userID].icon) //player has no icon? player is new. Player has icon? player already exists
            {
                bot.data[info.serverId][info.channelID].players[info.userID].icon = bot.data[info.serverId][info.channelID].icons.splice(0, 1);
                bot.data[info.serverId][info.channelID].players[info.userID].health = 0;
                bot.data[info.serverId][info.channelID].players[info.userID].actionTokens = 0;

                bot.sendMessage({
                    to: info.channelID,
                    message: "You've joined the fight!",
                    typing: false
                });
                return;
            }

            bot.sendMessage({
                to: info.channelID,
                message: "You are already in this fight!",
                typing: false
            });
        }
    },
    {
        cmd: "status",
        params: "none",
        category: "main",
        execute: function(bot, info, args)
        {
            VerifySave(bot, info);
            
            bot.data[info.serverId][info.channelID].players[info.userID] = bot.data[info.serverId][info.channelID].players[info.userID] || {}

            if(bot.data[info.serverId][info.channelID].players[info.userID].icon) //player has no icon? player is new. Player has icon? player already exists.
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: bot.data[info.serverId][info.channelID].players[info.userID].icon + " - <@!" + info.userID + ">'s Status:\nHealth: " + HealthBar(bot.data[info.serverId][info.channelID].players[info.userID].health, 3) + "\nTokens: " + HealthBar(bot.data[info.serverId][info.channelID].players[info.userID].actionTokens, 1),
                    typing: false
                });
                return;
            }

            bot.sendMessage({
                to: info.channelID,
                message: "<@" + info.userID + ">'s Status: Not Joined",
                typing: false
            });
        }
    },
    {
        cmd: "startgame", //Only needs to be called once in a new channel, from there on the game should automatically reset.
        params: "none",
        category: "main",
        execute: function(bot, info, args)
        {
            VerifySave(bot, info);
            
            if(!bot.data[info.serverId][info.channelID].enabled)
            {
                bot.data[info.serverId][info.channelID].enabled = true;

                bot.sendMessage({
                    to: info.channelID,
                    message: "<@" + info.userID + "> has started the fight in this channel!",
                    typing: false
                });
            }
            else
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "There already is an ongoing game here.",
                    typing: false
                });
            }
            
            
        }
    },
    {
        cmd: "checksave",
        params: "none",
        category: "main",
        execute: function(bot, info, args)
        {
            VerifySave(bot, info);
        }
    },
    {
        cmd: "invitelink",
        params: "none",
        category: "main",
        execute: function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "https://discordapp.com/oauth2/authorize?&client_id=869154086648430622&scope=bot&permissions=523328",
                typing: false
            });
        }
    },
    {
        cmd:"update",
        params: "none",
        hidden:true,
        category: "admin",
        execute:function(bot, info, args)
        {
            var exec = require('child_process').exec;

            bot.data.update = info.channelID;

            bot.sendMessage({
                to: info.channelID,
                message: "Fetching changes...",
                typing: false
            }, function()
            {
                exec('git fetch --all', function(err, stdout, stderr) 
                {
                    exec('git log --oneline master..origin/master', function(err, stdout, stderr) 
                    {
                        bot.sendMessage({
                            to: info.channelID,
                            message: (stdout == ""? "No changes, " : "Changes: ```" + stdout + "``` Updating and ") + "reloading!",
                            typing: false
                        }, function()
                        {
                            bot.suicide();
                        });
                    });
                });
            });
        }
    },
    {
        cmd: "version",
        params: "none",
        category: "main",
        execute: function(bot, info, args)
        {
            var exec = require('child_process').exec
            exec('git log --oneline -n 1 HEAD', function(err, stdout, stderr) 
            {
                console.log(stdout)
                bot.sendMessage({
                    to: info.channelID,
                    message: "Current version: ```" + stdout + "```",
                    typing:false
                })
            })
        }
    },
    {
        cmd: "dumpmem",
        params: "nothing",
        category: "admin",
        hidden: true,
        execute:function(bot, info, args)
        {
            var url = "botData.json";

            bot.sendMessage({
                to: info.channelID,
                message: "Dumping...",
                typing:false
            })

            bot.uploadFile({
                to: info.channelID,
                file: url
            }, function(error, response)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: error,
                    typing:false
                })
            })
        }
    },
]

exports.UpdateFunc = function (bot, info)
{
    VerifySave(bot, info);

    var count = 0;
    for (var k in bot.data[info.serverId][info.channelID].players) 
    {
        if (bot.data[info.serverId][info.channelID].players.hasOwnProperty(k)) 
        {
            count++;
        }
    }   

    if(count < 2)
        return;

    var today = new Date();
    bot.data[info.serverId][info.channelID].time = bot.data[info.serverId][info.channelID].time || {y: today.getFullYear(), m: today.getMonth(), d: today.getDate()}

    var date1 = new Date(bot.data[info.serverId][info.channelID].time.y, bot.data[info.serverId][info.channelID].time.m, bot.data[info.serverId][info.channelID].time.d);
    var date2 = new Date(today.getFullYear(), today.getMonth(), today.getDate());

    // To calculate the time difference of two dates
    var Difference_In_Time = date2.getTime() - date1.getTime();
    
    // To calculate the no. of days between two dates
    var Difference_In_Days = Math.round(Difference_In_Time / (1000 * 3600 * 24));
    
    bot.data[info.serverId][info.channelID].time = {y: today.getFullYear(), m: today.getMonth(), d: today.getDate()}

    for (var k in bot.data[info.serverId][info.channelID].players) 
    {
        if (bot.data[info.serverId][info.channelID].players.hasOwnProperty(k)) 
        {
            bot.data[info.serverId][info.channelID].players[k].actionTokens = bot.data[info.serverId][info.channelID].players[k].actionTokens || 0;
            bot.data[info.serverId][info.channelID].players[k].actionTokens += Difference_In_Days;

            bot.data[info.serverId][info.channelID].players[k].actionTokens = Math.round(bot.data[info.serverId][info.channelID].players[k].actionTokens);
        }
    }

    //Check if a game has ended
    var living = 0;
    var last = "";
    var allplayers = [];

    for (var k in bot.data[info.serverId][info.channelID].players) 
    {
        if (bot.data[info.serverId][info.channelID].players.hasOwnProperty(k)) 
        {
            var player = bot.data[info.serverId][info.channelID].players[k];
            allplayers.push("<@" + k + "> as " + player.icon);

            if(player.health > 0)
            {
                last = k;
                living ++;
            }
        }
    }

    if (living < 2 || bot.data[info.serverId][info.channelID].grid.width < 1 || bot.data[info.serverId][info.channelID].grid.height < 1)
    {
        if(last != "")
        {
            bot.sendMessage({
                to: info.channelID,
                message: "<@" + last + "> has won the fight in this channel!\nStarting a new game with: " + allplayers.join(", "),
                typing: false
            });
        }
        else
        {
            bot.sendMessage({
                to: info.channelID,
                message: "Starting a new game with: " + allplayers.join(", "),
                typing: false
            });
        }

        bot.data[info.serverId][info.channelID].grid = new grid(GridWidthForPlayerCount(count), GridHeightForPlayerCount(count))

        for (var k in bot.data[info.serverId][info.channelID].players) 
        {
            bot.data[info.serverId][info.channelID].players[k] = bot.data[info.serverId][info.channelID].players[k] || {};

            if (bot.data[info.serverId][info.channelID].players.hasOwnProperty(k)) 
            {
                var player = bot.data[info.serverId][info.channelID].players[k];

                player.health = 3;
                player.actionTokens = 1;

                var placed = false;

                while (!placed)
                {
                    var x = rand(0, GridWidthForPlayerCount(count));
                    var y = rand(0, GridHeightForPlayerCount(count));

                    if(bot.data[info.serverId][info.channelID].grid.GetPlayerIDAtPosition(x, y) == null)
                    {
                        bot.data[info.serverId][info.channelID].grid.SetPlayerIDAtPosition(x, y, k);
                        placed = true;
                    }
                }
            }
        }
    }

    for (var k in bot.data[info.serverId][info.channelID].players) 
    {
        if (bot.data[info.serverId][info.channelID].players.hasOwnProperty(k)) 
        {
            if(bot.data[info.serverId][info.channelID].players[k].health < 1)
                bot.data[info.serverId][info.channelID].players[k].actionTokens = 0;
        }
    }

    //From here on there is guaranteed to be a game on a grid with 2 or more living players, where only living players have action tokens.
}

function VerifySave(bot, info)
{
    bot.data[info.serverId] = bot.data[info.serverId] || {}
    bot.data[info.serverId][info.channelID] = bot.data[info.serverId][info.channelID] || {players:{}};
    bot.data[info.serverId][info.channelID].players = bot.data[info.serverId][info.channelID].players || {};
    bot.data[info.serverId][info.channelID].icons = bot.data[info.serverId][info.channelID].icons || shufflecopy(tiles.players);

    if(bot.data[info.serverId][info.channelID].enabled)
    {
        var count = 0;
        for (var k in bot.data[info.serverId][info.channelID].players) 
        {
            if (bot.data[info.serverId][info.channelID].players.hasOwnProperty(k)) 
            {
                ++count;
            }
        }

        bot.data[info.serverId][info.channelID].grid = bot.data[info.serverId][info.channelID].grid || new grid(GridWidthForPlayerCount(count), GridHeightForPlayerCount(count));
        bot.data[info.serverId][info.channelID].grid = Object.assign(new grid, bot.data[info.serverId][info.channelID].grid)
    }
}

function GridWidthForPlayerCount(count)
{
    return count+4;
}

function GridHeightForPlayerCount(count)
{
    return count+4;
}

function rand(min, max)
{
    return min + (Math.floor(Math.random() *(max - min)));
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function shufflecopy(a) {
    var n = [];
    for (var i = 0; i < a.length; i++) {
        n.push(a[i]);
    }

    n = shuffle(n);

    return n;
}

function CapitalizeFirstLetter(str)
{
    return str.charAt(0).toUpperCase() + str.slice(1);
}

function RollStat(statNum)
{
    return Math.round(rand(1, 10)) + statNum;
}

function HealthBar(current, max)
{
    var str = ""

    for(var i = 0; i < current; i++)
        str += "▰"

    for(var i = 0; i < max - current; i++)
        str += "▱";

    return str;
}

function saveTempAndUpload(bot, info, content)
{
    fileName = info.serverId + "-" + info.channelID + ".txt";

    fs.writeFile("./" + fileName, content, function(err) {
        if (err) {
            console.log(err);
        }

        bot.uploadFile({
            to: info.channelID,
            file: fileName
        }, (err, res) => 
        {
            setTimeout(function() 
            { 
                bot.deleteMessage({
                    channelID: info.channelID,
                    messageID: res.id
                });
            }, 60000);
        });

        fs.unlinkSync("./" + fileName) ;
    });
}