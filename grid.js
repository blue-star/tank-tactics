class grid
{
    width = 0;
    height = 0;

    grid = null;

    constructor(width, height)
    {
        this.width = width;
        this.height = height;

        this.ValidateGrid();
    }

    ValidateGrid()
    {
        if(this.grid == null)
        {
            this.grid = [];

            for(var x = 0; x < this.width; x++)
            {
                this.grid[x] = [];

                for(var y = 0; y < this.height; y++)
                {
                    this.grid[x][y] = null;
                }
            }
        }
    }

    GetPlayerIDAtPosition(x, y)
    {
        this.ValidateGrid();
        return this.grid[x][y];
    }

    SetPlayerIDAtPosition(x, y, id)
    {
        this.ValidateGrid();
        this.grid[x][y] = id;
    }
}

module.exports = grid;